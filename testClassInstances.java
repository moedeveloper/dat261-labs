/*
 * Copyright 2000-2017 JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.intellij.codeInsight.javadoc;

import com.intellij.lang.ASTNode;
import com.intellij.lang.Language;
import com.intellij.openapi.components.BaseComponent;
import com.intellij.openapi.extensions.ExtensionPointName;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Condition;
import com.intellij.openapi.util.Key;
import com.intellij.openapi.util.TextRange;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.scope.PsiScopeProcessor;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.search.SearchScope;
import com.intellij.util.IncorrectOperationException;
import com.intellij.util.messages.MessageBus;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.picocontainer.PicoContainer;

import javax.swing.*;

/**
 * Created by momor1 on 4/6/2017.
 */
public class testClassInstances {

  public static PsiElement getPsiElementInstance(){
    PsiElement psiElement = new PsiElement() {
      @NotNull
      @Override
      public Project getProject() throws PsiInvalidElementAccessException {
        return getProjectInstance();
      }

      @NotNull
      @Override
      public Language getLanguage() {
        return Language.ANY;
      }

      @Override
      public PsiManager getManager() {
        return null;
      }

      @NotNull
      @Override
      public PsiElement[] getChildren() {
        return new PsiElement[0];
      }

      @Override
      public PsiElement getParent() {
        return null;
      }

      @Override
      public PsiElement getFirstChild() {
        return null;
      }

      @Override
      public PsiElement getLastChild() {
        return null;
      }

      @Override
      public PsiElement getNextSibling() {
        return null;
      }

      @Override
      public PsiElement getPrevSibling() {
        return null;
      }

      @Override
      public PsiFile getContainingFile() throws PsiInvalidElementAccessException {
        return null;
      }

      @Override
      public TextRange getTextRange() {
        return null;
      }

      @Override
      public int getStartOffsetInParent() {
        return 0;
      }

      @Override
      public int getTextLength() {
        return 0;
      }

      @Nullable
      @Override
      public PsiElement findElementAt(int offset) {
        return null;
      }

      @Nullable
      @Override
      public PsiReference findReferenceAt(int offset) {
        return null;
      }

      @Override
      public int getTextOffset() {
        return 0;
      }

      @Override
      public String getText() {
        return null;
      }

      @NotNull
      @Override
      public char[] textToCharArray() {
        return new char[0];
      }

      @Override
      public PsiElement getNavigationElement() {
        return null;
      }

      @Override
      public PsiElement getOriginalElement() {
        return null;
      }

      @Override
      public boolean textMatches(@NotNull CharSequence text) {
        return false;
      }

      @Override
      public boolean textMatches(@NotNull PsiElement element) {
        return false;
      }

      @Override
      public boolean textContains(char c) {
        return false;
      }

      @Override
      public void accept(@NotNull PsiElementVisitor visitor) {

      }

      @Override
      public void acceptChildren(@NotNull PsiElementVisitor visitor) {

      }

      @Override
      public PsiElement copy() {
        return null;
      }

      @Override
      public PsiElement add(@NotNull PsiElement element) throws IncorrectOperationException {
        return null;
      }

      @Override
      public PsiElement addBefore(@NotNull PsiElement element, @Nullable PsiElement anchor) throws IncorrectOperationException {
        return null;
      }

      @Override
      public PsiElement addAfter(@NotNull PsiElement element, @Nullable PsiElement anchor) throws IncorrectOperationException {
        return null;
      }

      @Override
      public void checkAdd(@NotNull PsiElement element) throws IncorrectOperationException {

      }

      @Override
      public PsiElement addRange(PsiElement first, PsiElement last) throws IncorrectOperationException {
        return null;
      }

      @Override
      public PsiElement addRangeBefore(@NotNull PsiElement first, @NotNull PsiElement last, PsiElement anchor)
        throws IncorrectOperationException {
        return null;
      }

      @Override
      public PsiElement addRangeAfter(PsiElement first, PsiElement last, PsiElement anchor) throws IncorrectOperationException {
        return null;
      }

      @Override
      public void delete() throws IncorrectOperationException {

      }

      @Override
      public void checkDelete() throws IncorrectOperationException {

      }

      @Override
      public void deleteChildRange(PsiElement first, PsiElement last) throws IncorrectOperationException {

      }

      @Override
      public PsiElement replace(@NotNull PsiElement newElement) throws IncorrectOperationException {
        return null;
      }

      @Override
      public boolean isValid() {
        return false;
      }

      @Override
      public boolean isWritable() {
        return false;
      }

      @Nullable
      @Override
      public PsiReference getReference() {
        return null;
      }

      @NotNull
      @Override
      public PsiReference[] getReferences() {
        return new PsiReference[0];
      }

      @Nullable
      @Override
      public <T> T getCopyableUserData(Key<T> key) {
        return null;
      }

      @Override
      public <T> void putCopyableUserData(Key<T> key, @Nullable T value) {

      }

      @Override
      public boolean processDeclarations(@NotNull PsiScopeProcessor processor,
                                         @NotNull ResolveState state,
                                         @Nullable PsiElement lastParent,
                                         @NotNull PsiElement place) {
        return false;
      }

      @Nullable
      @Override
      public PsiElement getContext() {
        return null;
      }

      @Override
      public boolean isPhysical() {
        return false;
      }

      @NotNull
      @Override
      public GlobalSearchScope getResolveScope() {
        return null;
      }

      @NotNull
      @Override
      public SearchScope getUseScope() {
        return null;
      }

      @Override
      public ASTNode getNode() {
        return null;
      }

      @Override
      public boolean isEquivalentTo(PsiElement another) {
        return false;
      }

      @Override
      public Icon getIcon(int flags) {
        return null;
      }

      @Nullable
      @Override
      public <T> T getUserData(@NotNull Key<T> key) {
        return null;
      }

      @Override
      public <T> void putUserData(@NotNull Key<T> key, @Nullable T value) {

      }
    };
    return psiElement;
  }

  public static Project getProjectInstance(){

    Project project = new Project() {
      @NotNull
      @Override
      public String getName() {
        return "TestProject";
      }

      @Override
      public VirtualFile getBaseDir() {
        return null;
      }

      @Nullable
      @Override
      public String getBasePath() {
        return "BasePath";
      }

      @Nullable
      @Override
      public VirtualFile getProjectFile() {
        return null;
      }

      @Nullable
      @Override
      public String getProjectFilePath() {
        return "ProjectFilePath";
      }

      @Nullable
      @Override
      public String getPresentableUrl() {
        return "PresentableUrl";
      }

      @Nullable
      @Override
      public VirtualFile getWorkspaceFile() {
        return null;
      }

      @NotNull
      @Override
      public String getLocationHash() {
        return "LocationHash";
      }

      @Override
      public void save() {

      }

      @Override
      public boolean isOpen() {
        return false;
      }

      @Override
      public boolean isInitialized() {
        return false;
      }

      @Override
      public boolean isDefault() {
        return false;
      }

      @Override
      public BaseComponent getComponent(@NotNull String name) {
        return null;
      }

      @Override
      public <T> T getComponent(@NotNull Class<T> interfaceClass) {
        return null;
      }

      @Override
      public <T> T getComponent(@NotNull Class<T> interfaceClass, T defaultImplementationIfAbsent) {
        return null;
      }

      @Override
      public boolean hasComponent(@NotNull Class interfaceClass) {
        return false;
      }

      @NotNull
      @Override
      public <T> T[] getComponents(@NotNull Class<T> baseClass) {
        return null;
      }

      @NotNull
      @Override
      public PicoContainer getPicoContainer() {
        return null;
      }

      @NotNull
      @Override
      public MessageBus getMessageBus() {
        return null;
      }

      @Override
      public boolean isDisposed() {
        return false;
      }

      @NotNull
      @Override
      public <T> T[] getExtensions(@NotNull ExtensionPointName<T> extensionPointName) {
        return null;
      }

      @NotNull
      @Override
      public Condition<?> getDisposed() {
        return null;
      }

      @Override
      public void dispose() {

      }

      @Nullable
      @Override
      public <T> T getUserData(@NotNull Key<T> key) {
        return null;
      }

      @Override
      public <T> void putUserData(@NotNull Key<T> key, @Nullable T value) {

      }
    };
    return project;
  }
}
