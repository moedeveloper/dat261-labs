/*
 * Copyright 2000-2017 JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.intellij.codeInsight.javadoc;
import com.intellij.lang.ASTNode;
import com.intellij.lang.Language;
import com.intellij.openapi.components.BaseComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.extensions.ExtensionPointName;
import com.intellij.openapi.project.Project;
import com.intellij.codeInsight.documentation.AbstractExternalFilter;
import com.intellij.icons.AllIcons;
import com.intellij.openapi.util.Condition;
import com.intellij.openapi.util.Key;
import com.intellij.openapi.util.TextRange;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileSystem;
import com.intellij.psi.*;
import com.intellij.psi.scope.PsiScopeProcessor;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.search.SearchScope;
import com.intellij.util.IncorrectOperationException;
import com.intellij.util.messages.MessageBus;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.builtInWebServer.BuiltInServerOptions;
import org.picocontainer.PicoContainer;

import javax.swing.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by momor1 on 4/5/2017.
 */
class JavaDocExternalFilterTest {

  @org.junit.jupiter.api.Test
  void getRefConverters() {

  }

  @org.junit.jupiter.api.Test
  void filterInternalDocInfo() {

    Project pro = null;

    String testString = JavaDocExternalFilter.filterInternalDocInfo("test");
    assertEquals("test",testString);
  }

  @org.junit.jupiter.api.Test
  void getExternalDocInfoForElement() throws Exception {
    BuiltInServerOptions ps = new BuiltInServerOptions();

    PsiElement psi = testClassInstances.getPsiElementInstance();
    Project project = testClassInstances.getProjectInstance();


    JavaDocExternalFilter sj = new JavaDocExternalFilter(project);

    String rsult = sj.getExternalDocInfoForElement("http://localhost:8080/Test", psi);

    assertEquals("test", rsult);

  }

  @org.junit.jupiter.api.Test
  void getParseSettings() {
  }
}