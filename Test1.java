/*
 * Copyright 2000-2017 JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.intellij.html;

import com.intellij.codeInsight.documentation.DocumentationComponent;
import com.intellij.codeInsight.documentation.DocumentationManager;
import com.intellij.codeInsight.javadoc.JavaDocExternalFilter;
import com.intellij.lang.java.JavaLanguage;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiFileFactory;
import com.intellij.testFramework.PlatformTestCase;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Geek on 4/5/2017.
 */
/* @System Under Test --Documentation Manager
 *  We are going to test the Quick Documentation for HTML files.
*/

public class Test1 extends PlatformTestCase { //LightPlatformCodeInsightTestCase {


    @Before
    // Extend PlatformTestCase

    @Test

    public void testFilterInternalDocInfo() {
        String testString = JavaDocExternalFilter.filterInternalDocInfo("testing for fun");
        String nullTest = JavaDocExternalFilter.filterInternalDocInfo(null);
        assertEquals("testing for fun", testString);
        assertEquals(null, null);
    }

    public void testGetDocument() {
        PsiFile psiFile = PsiFileFactory.getInstance(myProject).createFileFromText(JavaLanguage.INSTANCE, "testing for fun");
        Project project = psiFile.getProject();
        Document document = PsiDocumentManager.getInstance(project).getDocument(psiFile);
        assertNotNull(document);
    }

    public void testisEmpty() {
        PsiFile psiFile = PsiFileFactory.getInstance(myProject).createFileFromText(JavaLanguage.INSTANCE, "testing for fun");
        Project project = psiFile.getProject();
        DocumentationManager documentationManager = DocumentationManager.getInstance(project);
        DocumentationComponent documentationComponent = new DocumentationComponent(documentationManager);
        assertEquals(true, documentationComponent.isEmpty());
    }

    public void testRequestFocusInWindow() {
        PsiFile psiFile = PsiFileFactory.getInstance(myProject).createFileFromText(JavaLanguage.INSTANCE, "testing for fun");
        Project project = psiFile.getProject();
        DocumentationManager documentationManager = DocumentationManager.getInstance(project);
        DocumentationComponent documentationComponent = new DocumentationComponent(documentationManager);
        assertEquals(false, documentationComponent.requestFocusInWindow());
    }
}